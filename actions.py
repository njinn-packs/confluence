import json
import logging
import re
from string import Template
from time import sleep
from urllib.parse import urlparse

import requests
from atlassian import Confluence
import xmltodict


class ConfluenceBase:
    def __init__(self):
        self.connection = dict()
        self.__confluence_instance = None

    @property
    def confluence(self):
        if not self.__confluence_instance:
            self.__confluence_instance = self.confluence_instance()
        return self.__confluence_instance

    def confluence_instance(self):
        if self.connection and self.connection.get("url"):
            self.url = self.connection.get("url")
            self.parsed_uri = urlparse(self.url)

            if self.connection.get("cloud", False):
                confluence = Confluence(
                    url=self.url,
                    username=self.connection.get("username"),
                    password=self.connection.get("password"),
                    api_version="cloud",
                )
            else:
                confluence = Confluence(
                    url=self.url,
                    username=self.connection.get("username"),
                    password=self.connection.get("password"),
                )
        else:
            raise Exception("No connection information provided")

        return confluence


class ExportPDF(ConfluenceBase):
    """
    Trigger PDF Export of a page and return it's content
    add option to attach pdf to page as attachment?
    """

    page_id = None
    file_name = None

    def run(self):
        if not self.page_id:
            raise Exception("Page ID is required")

        if not self.file_name:
            file_name = f"{self.page_id}.pdf"
        else:
            if re.findall(r"[^A-Za-z0-9_\-\.\ ]", self.file_name):
                raise Exception(
                    f"Invalid file name {self.file_name} provided, must only contain [A-Za-z0-9_-. ]"
                )
            file_name = self.file_name

        print(f"Exporting page ({self.page_id})")
        content = self.confluence.export_page(self.page_id)

        with open(file_name, "wb") as pdf_file:
            pdf_file.write(content)
            pdf_file.close()

        return {"file": self._njinn.upload_file(file_name)}


class SearchPage(ConfluenceBase):
    """
    Search for a page via CQL expression and return its id
    """

    cql = None

    def run(self):

        pages = []

        if not self.cql:
            raise Exception("No CQL expression defined")

        res = self.confluence.cql(self.cql)

        if not res.get("results"):
            raise Exception(res)

        for r in res["results"]:
            pages.append(r["content"]["id"])

        print(f"Found {len(pages)} match(es)")
        print(pages)

        return {"pages": pages, "response": res}


class UpdatePage(ConfluenceBase):
    """
    Update page content and title
    """

    title = None
    content = None
    page_id = None

    def run(self):
        if not self.page_id:
            raise Exception("Page ID is required")

        page = self.confluence.get_page_by_id(self.page_id)

        if not self.title:
            self.title = page.get("title", "")

        page_updated = self.confluence.update_page(
            parent_id=None, page_id=self.page_id, title=self.title, body=self.content
        )

        if not page_updated.get("id"):
            raise Exception(page_updated)

        return {"page": page_updated}


class AttachFile(ConfluenceBase):
    """
    Attach a file to a page
    """

    page_id = None
    file_name = None

    def run(self):
        if not self.page_id:
            raise Exception("Page ID is required")

        if not self.file_name:
            raise Exception("File name to upload is required")

        print(f"Attaching file {self.file_name} to page (id:{self.page_id})")
        attachments = self.confluence.attach_file(
            self.file_name, self.page_id, comment="Uploaded by an njinn task"
        )

        # response should be content array for creation
        if not attachments.get("results") or attachments.get("size") == 0:
            # response should be content details for update
            if not attachments.get("type") == "attachment":
                raise Exception(attachments)

        return {"attachments": attachments}


class CreateUpdatePage(ConfluenceBase):
    """
    Create or update a page
    """

    parent_id = ""
    title = ""
    content = ""

    def run(self):
        if not self.parent_id:
            raise Exception("Parent page ID is required")

        if not self.title:
            raise Exception("Page title is required")

        print(
            f"Creating or updating page '{self.title}' for parent page (id:{self.parent_id})"
        )
        page = self.confluence.update_or_create(
            self.parent_id, self.title, self.content, representation="storage"
        )

        return {"page": page}


class DuplicatePage(ConfluenceBase):
    """
    Duplicates page and optionally replaces placeholders in body and title following the python template string notation (https://docs.python.org/3/library/string.html#template-strings)
    """

    page_id = None
    mapping = None
    parent_id = None

    def run(self):
        if not self.page_id:
            raise Exception("Page ID is required")

        if not self.parent_id:
            self.parent_id = self.page_id

        print(f"Retrieving content of page:{self.page_id}")
        page = self.confluence.get_page_by_id(
            page_id=self.page_id, expand="body.storage,space"
        )

        if not page.get("body"):
            raise Exception(page)

        body_new = page["body"]["storage"]["value"]
        title_old = page["title"]
        title_new = title_old

        if self.mapping:
            mapping = json.loads(self.mapping)
            print(f"Applying mapping {mapping} to page title and content")
            t_body = Template(page["body"]["storage"]["value"])
            t_title = Template(page["title"])
            body_new = t_body.safe_substitute(mapping)
            title_new = t_title.safe_substitute(mapping)

        if title_new == title_old:
            title_new += " - duplicate"

        print(f"Creating new page")
        page_new = self.confluence.create_page(
            space=page["space"]["key"],
            title=title_new,
            body=body_new,
            parent_id=self.parent_id,
        )

        if not page_new and not page_new.get("id"):
            raise Exception(page_new)

        return {"page": page_new}


class GetMentions(ConfluenceBase):
    """
    Get a list of users (keys) mentioned on a page
    """

    page_id = None

    def run(self):
        if not self.page_id:
            raise Exception("Page ID is required")

        page = self.confluence.get_page_by_id(
            page_id=self.page_id, expand="body.storage"
        )

        pattern = '~(.*?)(?=">)'
        mentions = []
        mentions = re.findall(pattern, page["body"]["storage"]["value"])

        return mentions


class CreateSpace(ConfluenceBase):
    """
    Create private or public space
    """

    key = ""
    name = ""
    private = False

    def run(self):
        if not self.key or not self.key.isalnum():
            raise Exception(
                "A valid key must be provided (alphanumeric chars only, max. length 255 chars"
            )

        if not self.name:
            self.name = "Created by Njinn Automation"

        print(f"Creating space (key:{self.key}, name:{self.name})")
        if self.private:
            print(f"It's a private space for user {self.connection['username']}")
            data = {"key": self.key, "name": self.name}
            self.confluence.post("rest/api/space/_private", data=data)
        else:
            self.confluence.create_space(self.key, self.name)
        print("Done.")

        return {"key": self.key, "name": self.name}


class CreatePersonalSpace(ConfluenceBase):
    """
    Create personal space for user (Confluence server only, not supported in Confluence cloud)
    """

    username = ""

    def run(self):
        if not self.username:
            raise Exception("No username provided")

        print(f"Creating personal space for user {self.username}")
        url = self.connection["url"] + "/rpc/soap-axis/confluenceservice-v2?wsdl"
        headers = {"Content-Type": "application/soap+xml", "SOAPAction": ""}

        data_login = f"""<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://soap.rpc.confluence.atlassian.com">
        <soapenv:Header/>
        <soapenv:Body>
            <soap:login soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                <in0 xsi:type="xsd:string">{self.connection['username']}</in0>
                <in1 xsi:type="xsd:string">{self.connection['password']}</in1>
            </soap:login>
        </soapenv:Body>
        </soapenv:Envelope>"""

        res_login = requests.post(url, data_login, headers=headers)

        try:
            res_login_dict = xmltodict.parse(res_login.text)
            auth_token = res_login_dict["soapenv:Envelope"]["soapenv:Body"][
                "ns1:loginResponse"
            ]["loginReturn"]["#text"]
        except Exception as e:
            print("Confluence server did not reply as expected. Response received:")
            print(res_login.text)
            raise e

        data_create_space = f"""<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://soap.rpc.confluence.atlassian.com">
        <soapenv:Header/>
        <soapenv:Body>
            <soap:addPersonalSpaceWithDefaultPermissions soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                <in0 xsi:type="xsd:string">{auth_token}</in0>
                <in1 xsi:type="bean:RemoteSpace" xmlns:bean="http://beans.soap.rpc.confluence.atlassian.com">
                    <name xsi:type="xsd:string">{self.username}</name>
                    <type xsi:type="xsd:string">personal</type>
                    <spaceGroup xsi:type="xsd:string" xsi:nil="true"/>
                </in1>
                <in2 xsi:type="xsd:string">{self.username}</in2>
            </soap:addPersonalSpaceWithDefaultPermissions>
        </soapenv:Body>
        </soapenv:Envelope>"""

        res_createspace = requests.post(url, data_create_space, headers=headers)
        try:
            res_createspace_dict = xmltodict.parse(res_createspace.text)
            space_url = res_createspace_dict["soapenv:Envelope"]["soapenv:Body"][
                "ns1:addPersonalSpaceWithDefaultPermissionsResponse"
            ]["addPersonalSpaceWithDefaultPermissionsReturn"]["url"]["#text"]
        except Exception as e:
            print("Confluence server did not reply as expected. Response received:")
            print(res_createspace.text)
            raise e

        print(f"Space url: {space_url}")
        return {"url": space_url}
